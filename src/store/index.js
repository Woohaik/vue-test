import Vue from 'vue'
import Vuex from 'vuex'
import catModule from "./cats"
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    catModule
  }
})

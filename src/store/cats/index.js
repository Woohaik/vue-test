import axios from "axios";

const state = {
    cats: []
}

const getters = {
    cats: state => state.cats,
}


const actions = {


    async fetchCat({ commit }, name) {
        try {
            const cat = await (await axios.get("https://api.thecatapi.com/v1/images/search"))

            if (!cat) {
                throw new Error("Cat error :( ")
            }



            commit("ADD_CAT", { url: cat.data[0].url, id: cat.data[0].id, name })

        } catch (err) {
            console.log(err);
        }
    }
}

const mutations = {
    ADD_CAT(state, payload) {
        state.cats.push(payload)
    }
}






export default {
    state,
    getters,
    actions,
    mutations,
    namespaced: true,
}
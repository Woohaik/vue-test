import Vue from 'vue'
import App from './App.vue'
import store from './store'
import axios from "axios"

import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";


Vue.prototype.$http = axios;



Vue.config.productionTip = false


axios.defaults.baseURL = "http://localhost:8080/";
axios.defaults.headers.common["x-api-key"] = "5b81395f-e824-4614-bea6-7e5ad907e5d7";

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')

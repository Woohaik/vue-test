// https://docs.cypress.io/api/introduction/api.html





describe('My First Test', () => {
  before(() => {
    cy.visit('http://localhost:8080')
  })

  it('Visits the app ', () => {
    cy.contains('h1', 'Supra kat')
    cy.get("input#catName").type('Super gatungo', { delay: 200 });
    cy.get("button#addCatButton").click();
  })


  it("The new cat is there", () => {
    cy.get("div.catB div.name").first().should("contain", "Super gat")
  })


  it("heart the cat", () => {
    cy.wait(500);
    cy.get("div.catB .heart-container").first().click()
    cy.wait(100);
  })

  it("Verify click", () => {
    cy.get("div.catB .heart-container").first().should("have.class", "red")
    cy.wait(100);
  })



})

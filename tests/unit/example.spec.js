import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import catBlock from '@/components/CatBlock.vue'

describe('CatBlock.vue', () => {
  it('Render URL', () => {
    const url = "https://cdn2.thecatapi.com/images/9c8.jpg"
    const wrapper = shallowMount(catBlock, {
      propsData: { url }
    })

    expect(wrapper.text()).to.equal("");
    expect(wrapper.html()).to.include('src="https://cdn2.thecatapi.com/images/9c8.jpg"')
  })
})

